<?php
$adjective = 'adjetive';
$code = 'code';
echo 'phptherightway is ' . $adjective . '.'     // a single quotes example that uses multiple concatenating for
    . "\n"                                       // variables and escaped string
    . 'I love learning' . $code . '!';

// vs

echo "phptherightway is $adjective.\n I love learning $code!";  // Instead of multiple concatenating, double quotes
                                                               // enables us to use a parsable string