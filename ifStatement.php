<?php
function testIfElse($a)
{
    if ($a) {
        return true;
    } else {
        return false;
    }
}

// vs.

function testJustIf($a)
{
    if ($a) {
        return true;
    }
    return false;    // else is not necessary
}

// or even shorter:

function test($a)
{ 
    return (bool) $a;
}
