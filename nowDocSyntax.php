<?php
// $str = <<<'EOD'           // initialized by <<<
// Example of string
// spanning multiple lines
// using nowdoc syntax.
// $a does not parse.
// EOD;                        // closing 'EOD' must be on it's own line, and to the left most point

/**
 * Output:
 *
 * Example of string
 * spanning multiple lines
 * using nowdoc syntax.
 * $a does not parse.
 */