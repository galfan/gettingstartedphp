<?php
$a = 5;
echo ($a == 5) ? 'yay' : 'nay';

echo ($a) ? ($a == 5) ? 'yay' : 'nay' : ($b == 10) ? 'excessive' : ':(';    // excess nesting, sacrificing readability

// echo ($a != 3 && $b != 4) || $c == 5;