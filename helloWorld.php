<?php
    if($argc !== 2) {
        echo "Usage: php hello.php <name>. \n";
        exit(1);
    }
    $name = $argv;
    echo "Hello, $name \n";