<?php
echo "
something
about 
you";

// $str = <<< EOD               // initialized by <<<
// Example of string
// spanning multiple lines
// using heredoc syntax.
// $a are parsed.
// EOD;                        // closing 'EOD' must be on it's own line, and to the left most point

/**
 * Output:
 *
 * Example of string
 * spanning multiple lines
 * using heredoc syntax.
 * Variables are parsed.
 */